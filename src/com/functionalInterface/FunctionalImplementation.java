package com.functionalInterface;

public class FunctionalImplementation {
	public static void main(String[] args) {
		FuncInterface fi = () -> System.out.println("Hello");
		fi.sayHello();
	}
}
