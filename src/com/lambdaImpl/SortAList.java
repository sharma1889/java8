package com.lambdaImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortAList {

	public static void main(String[] args) {
		List l1=new ArrayList<Integer>();
		l1.add(10);
		l1.add(0);	
		l1.add(34);
		l1.add(4);
		Collections.sort(l1, (Integer o1, Integer o2) ->  (o1>o2)?1 :(o1<o2)?-1:0);

		System.out.println(l1);
	}
}
