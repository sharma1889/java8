package com.lambdaImpl.customSorter;

import java.util.ArrayList;
import java.util.Collections;

public class CustomClassLambda {

	public static void main(String[] args) {
		ArrayList<Employee> al = new ArrayList<Employee>();
		al.add(new Employee(10, "Deepak"));
		al.add(new Employee(01, "Vivek"));
		al.add(new Employee(5, "Mohan"));
		al.add(new Employee(2, "Ram"));
		System.out.println("before--"+al);

		Collections.sort(al, (Employee I1,Employee I2) -> (I1.getId()<I2.getId())? -1: (I1.getId()>I2.getId())? 1: 0);
		System.out.println("after--"+al);

	}
}
